
import datetime
from datetime import datetime, date
from time import time
import math
import importlib

import keras
from keras.models import Sequential
from keras.models import Model
from keras.layers import Conv2D, MaxPooling2D
from keras.layers import Activation, Dropout, Flatten, Dense, GlobalAveragePooling2D
from keras.layers.normalization import BatchNormalization
from keras import regularizers
from keras.preprocessing.image import ImageDataGenerator, array_to_img, img_to_array, load_img
from keras.utils.data_utils import get_file
from keras import optimizers
from keras.utils import plot_model

from keras import backend as K
import numpy as np
import cv2
import os
import os.path

def timeNowString():
    return str(datetime.now().strftime('%y_%m_%d_%H_%M'))


# Database directory location
trainDirPath = '../DB/256_ObjectCategories'
# weights are saved in this directory
weightsDir = './weights/'
# logs used by tensorboard
log_dir = "logs/" # you can rename the folder after the training is complete for achiving purposes
# log_dir = "" # To disable train logging (please don't)

# Load weights? (to resume training)
# keep this string empty if you want to train from the beggining
weightsFilename = '' # 
# weightsFilename = 'mobilenet-18_11_28_01_10-136-0.5800.h5'
# weightsFilename = 'inception_resnet_v2-18_11_28_10_55-03-0.4106.h5' 

lr = 0.001
# optimizer='rmsprop'
optimizer=optimizers.Adam(lr=lr, beta_1=0.9, beta_2=0.999, epsilon=1e-08, decay=0.0)
# optimizer=optimizers.RMSprop(lr=0.005, rho=0.9, epsilon=None, decay=0.0)

# number of epochs to train
# don't worry the model is saved every epoch you can stop anytime
epochs = 200000
# Don't use batch size larger than 256
batch_size = 8  # limited by GPU memory
# If you see this:
# The caller indicates that this is not a failure, but may mean that there could be performance gains if more memory were available.
# lower the number

# Data augmentation configuration for training
train_datagen = ImageDataGenerator(
    rescale=1./255,
    rotation_range=40,
    width_shift_range=0.2,
    height_shift_range=0.2,
    shear_range=0.2,
    zoom_range=0.2,
    horizontal_flip=True,
    fill_mode='nearest')

# Data augmentation configuration for testing
test_datagen = ImageDataGenerator(rescale=1./255)

# number of classes calculated from the number of database subdirectories

# os.walk but only up til recursion level = 'level'
def walklevel(some_dir, level=1): 
    some_dir = some_dir.rstrip(os.path.sep)
    assert os.path.isdir(some_dir)
    num_sep = some_dir.count(os.path.sep)
    for root, dirs, files in os.walk(some_dir):
        yield root, dirs, files
        num_sep_this = root.count(os.path.sep)
        if num_sep + level <= num_sep_this:
            del dirs[:]

num_classes = len([x[0] for x in walklevel(trainDirPath)]) -1 # -1 because of '.'
print('\nNumber of classes found: ' +  str(num_classes))

weights = 'imagenet'
# weights = None
# one of `None` (random initialization),
#        'imagenet' (pre-training on ImageNet),
#        or the path to the weights file to be loaded.

# if using transfer learning
# the weight loading only works with the number of classes it was trained on
if weights == 'imagenet':
    # default number of classes, will be adjusted to your 'num_classes' later
    model_num_classes = 1000
else:
    model_num_classes = num_classes

####
# import modules from other directories
def module_from_file(module_name, file_path):
    spec = importlib.util.spec_from_file_location(module_name, file_path)
    module = importlib.util.module_from_spec(spec)
    spec.loader.exec_module(module)
    return module
####
# Create model
# Choose to uncomment one of the following
#
## inception_v4
#
# inception_v4 = module_from_file("inception_v4", "../keras-inceptionV4/inception_v4.py")
# modelName = 'inception_v4'
# image_size = 299
# model = inception_v4.create_model(
#     num_classes=model_num_classes, dropout_prob=0.2, weights=weights,  include_top=True, image_size=image_size)
#
## inception_resnet_v2
#
modelName = 'inception_resnet_v2'
image_size = 299
model = keras.applications.inception_resnet_v2.InceptionResNetV2(
    include_top=True, weights=weights, input_tensor=None,
    input_shape=None, pooling=None, classes=model_num_classes)
#
## MobileNetV2
#
# modelName = 'mobilenet_v2'
# image_size = 224
# model = keras.applications.mobilenet_v2.MobileNetV2(
#     input_shape=None, alpha=1.0, depth_multiplier=1,
#     include_top=True, weights=weights, input_tensor=None, pooling=None, classes=model_num_classes)
#
## MobileNet
#
# modelName = 'mobilenet'
# image_size = 224
# model = keras.applications.mobilenet.MobileNet(
#     input_shape=None, alpha=1.0, depth_multiplier=1,
#     dropout=1e-3, include_top=True, weights=weights,
#     input_tensor=None, pooling=None, classes=model_num_classes)
#
## MobileNetV2 github
#
# mobilenet_v2 = module_from_file("mobilenet_v2", "../MobileNetV2/mobilenet_v2.py")
# modelName = 'mobilenet_v2_github'
# image_size = 224
# model = mobilenet_v2.MobileNetv2((image_size, image_size, 3), model_num_classes)


def pop_layer(model):
    if not model.outputs:
        raise Exception('Sequential model cannot be popped: model is empty.')
    model.layers.pop()
    if not model.layers:
        model.outputs = []
        model.inbound_nodes = []
        model.outbound_nodes = []
    else:
        model.layers[-1].outbound_nodes = []
        model.outputs = [model.layers[-1].output]
    model.built = False
####
# if using transfer learning
# Add custom trainable layers and set output size to your chosen 'num_classes'
if weights == 'imagenet':
    for layer in model.layers:
        layer.trainable = False  # disable training the pre-trained network
    x = model.layers[-2].output
    # Adding custom Layers
    # we add dense layers so that the model can learn more complex functions and classify for better results.

    x = Flatten()(x)

    x = Dense(1024, kernel_initializer='he_uniform',
              kernel_regularizer=regularizers.l2(0.01),
              activity_regularizer=regularizers.l1(0.01))(x)
    x = BatchNormalization(momentum=0.9997, scale=False)(x)
    x = Activation('relu')(x)
    x = Dropout(0.5)(x)
    x = Dense(512, kernel_initializer='he_uniform',
              kernel_regularizer=regularizers.l2(0.01),
              activity_regularizer=regularizers.l1(0.01))(x)
    x = BatchNormalization(momentum=0.9997, scale=False)(x)
    x = Activation('relu')(x)

    # Your new output layer with 'num_classes'
    predictions = Dense(num_classes, activation="softmax")(x)
    # create the final model
    model = Model(inputs=model.input, outputs=predictions)

# Make sure weightsDir exists, if it doesn't the model won't be saved!
if not os.path.exists(weightsDir):
    os.makedirs(weightsDir)

# Load weights? (to resume training)
if(weightsFilename is not None and weightsFilename != ''):
    model.load_weights(weightsDir + weightsFilename, by_name=False)

####
# Plot model

# plot_model(model, to_file='model.png', show_shapes=True)

####
# Callbacks
callbacks = []
#
# Checkpoint the model
#
checkpointCallback = keras.callbacks.ModelCheckpoint(
    weightsDir + modelName + '-'
    + timeNowString() +
    '-{epoch:03d}-{val_loss:.4f}' + '.h5',
    monitor='val_loss', verbose=0, save_best_only=False, save_weights_only=True, mode='auto', period=1)

callbacks.append(checkpointCallback)
#
# LearningRateScheduler
#
def step_decay(epoch):
   initial_lrate = lr
   drop = 9.6/10
   epochs_drop = 1
   lrate = initial_lrate * math.pow(drop, math.floor((1+epoch)/epochs_drop))
   return lrate

lrateCallback = keras.callbacks.LearningRateScheduler(step_decay)

callbacks.append(lrateCallback)
#
# TensorBoard
#
if(log_dir != ""):
    log_dir = log_dir + timeNowString() + "-" + modelName
    tensorboard = keras.callbacks.TensorBoard(log_dir=log_dir)
    callbacks.append(tensorboard)
#
# EarlyStopping
#
callbackEarlyStopping = keras.callbacks.EarlyStopping(
    monitor='val_loss', min_delta=0, patience=4, verbose=1, mode='auto', baseline=None)

callbacks.append(callbackEarlyStopping)
#
####
# Compile
model.compile(loss='categorical_crossentropy',
              optimizer=optimizer,
              metrics=['accuracy'])

####
# Save architecture
model.save(weightsDir + modelName + timeNowString() + '.full.h5')

####
# Database location
valDirPath = trainDirPath
testDirPath = trainDirPath

# this is a generator that will read pictures found in
# subfolers of 'data/train', and indefinitely generate
# batches of augmented image data
train_generator = train_datagen.flow_from_directory(
    trainDirPath,  # this is the target directory
    target_size=(image_size, image_size),  # all images will be resized to 150x150
    batch_size=batch_size,
    class_mode='categorical')

# this is a similar generator, for validation data
validation_generator = test_datagen.flow_from_directory(
    valDirPath,
    target_size=(image_size, image_size),
    batch_size=batch_size,
    class_mode='categorical')

# Start training

# number of files in the database
steps_per_epoch = sum([len(files) for r, d, files in os.walk(trainDirPath)])

model.fit_generator(
    train_generator,
    steps_per_epoch=(steps_per_epoch) // batch_size,
    epochs=epochs,
    validation_data=validation_generator,
    validation_steps=(batch_size*6) // batch_size,
    callbacks=callbacks)

# save after training is complete
model.save(weightsDir + modelName + timeNowString() + '.end.h5')
