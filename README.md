# InceptionDogs

## Train your very own network

### Database

To start you will need to aquire a database

Find files that represent your classes and put them on a subfolder like so:

```bash
.
├── downloads
│   ├── Aqua konosuba
│   ├── directoryToPng.py
│   ├── Emilia re zero
│   ├── kancolle amatsukaze
│   ├── Marisa touhou
│   ├── Megumin
│   ├── Miku
│   ├── Reimu
│   ├── Senjougahara
│   ├── Tohsaka fate
│   └── Zero Two
├── README.md
├── train.py
```

Where each subfolder of downloads has pictures of the class of the folder's name, e.g. the folder ./downloads/Megumin has pictures of megumin

Don't mind the specific names, you can name your directory 'database' instead of 'downloads'

An easy way to get such pictures is using [Google Images Download](https://github.com/hardikvasa/google-images-download)

to do so just `pip install google_images_download`
then for every class you want call
`googleimagesdownload  --keywords "kancolle amatsukaze" --limit 50`
this will download the pictures google images finds by searching "Marisa touhou" and put them on ./downloads/Marisa touhou

Then use 